package compressedtexture;

public abstract class CompressedImage
{
	public abstract int getWidth();
	public abstract int getHeight();
	public abstract int getNumMipMaps();
}
